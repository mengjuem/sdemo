from cffi import FFI

ffi = FFI()

ffi.set_source("_demo", "",
  include_dirs=["."],
  libraries=["PylonC_MD_VC120"],
  sources=["demo_source.c"]
)


if __name__ == "__main__":
  import setuptools
  ffi.compile(verbose=True)
